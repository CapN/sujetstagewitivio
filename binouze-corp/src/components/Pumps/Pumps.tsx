import React from 'react';
import BeerPump, { BeerPumpProps } from '../BeerPump/BeerPump';
import Pump from '../../interfaces/Pump';

type PumpsProps = { pumps: Pump[], incrementOutflow: Function, decrementOutflow: Function };

const Pumps: React.FC<PumpsProps> = ({ pumps, incrementOutflow, decrementOutflow }) => {

    const pumpsComponents = pumps.map((pump,pumpId) => <BeerPump
        outflow={pump.outflow}
        beerType={pump.beerType}
        incrementOutflow={()=>incrementOutflow(pumpId)}
        decrementOutflow={()=>decrementOutflow(pumpId)}
         />);
    return (
        <div className="App">
            {pumpsComponents}
        </div>
    );
}

export default Pumps;

