import React from 'react';
import './BeerPump.css';


export type BeerPumpProps = { outflow: number, beerType: string, incrementOutflow: Function,decrementOutflow: Function };

const BeerPump: React.FC<BeerPumpProps> = ({ outflow, beerType, incrementOutflow, decrementOutflow }) => {

    return (
        <div className="beer-pump">
            <div className="beer-pump-type"><span className="type">Type :</span>
                <span>{beerType}</span>
            </div>
            <div className="beer-pump-outflow"><span className="outflow">Outflow :</span>
            <button onClick={() => decrementOutflow()}>-</button>
            <span>{outflow}</span>
            <button onClick={() => incrementOutflow()}>+</button>

            </div>
        </div>
    )
}

export default BeerPump;

