import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Pumps from './components/Pumps/Pumps';

const defaultFactory = [
  { outflow: 0, beerType: "Blonde" },
  { outflow: 0, beerType: "Brune" },
  { outflow: 0, beerType: "Ambrée" }
];

const App = () => {
  const [count, setCount] = useState(defaultFactory.length);
  const [factory, setFactory] = useState(defaultFactory);

  const addPump = () => {
    const newBeer: string = prompt("Pump type: ", "Noel") as string;
    setFactory(oldFactory => {
      const tmpFactory = [...oldFactory];
      tmpFactory.push({ outflow: 0, beerType: newBeer });
      return tmpFactory;
    });
    setCount(defaultFactory.length);
  }

  const incrementOutflow = (pumpId: number) => {
    setFactory(oldFactory => {
      const tmpFactory = [...oldFactory];
      tmpFactory[pumpId].outflow += 1;
      return tmpFactory;
    });
  }
  const decrementOutflow = (pumpId: number) => {
    if (factory[pumpId].outflow > 0) {
      setFactory(oldFactory => {
        const tmpFactory = [...oldFactory];
        tmpFactory[pumpId].outflow -= 1;
        return tmpFactory;
      });
    }
  }
  return (
    <div className="App">
      <header className="App-header">
        <h3>Binouze Corp.</h3>
      </header>
      <Pumps
        pumps={factory}
        incrementOutflow={incrementOutflow}
        decrementOutflow={decrementOutflow} />
      <p>number of pumps : {count}</p>
      <button onClick={() => addPump()}> add Pump</button>
    </div>
  );
}

export default App;
